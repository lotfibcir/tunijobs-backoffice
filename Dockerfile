# Use the official Node.js image as the base image
FROM node:18

# Set the working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install the required dependencies
RUN npm install

# Copy the rest of the application code to the working directory
COPY . .

# Build the Angular app
RUN npm run build --prod

# Use the official nginx image to serve the built app
FROM nginx:latest

# Copy the built app to the nginx html folder
COPY --from=0 /app/dist/out /usr/share/nginx/html

# Expose port 80 for nginx
EXPOSE 80

# Start nginx
CMD ["nginx", "-g", "daemon off;"]

# Copy the custom nginx configuration
COPY nginx.config /etc/nginx/conf.d/default.conf


