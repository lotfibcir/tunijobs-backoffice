export interface AppUser{
  id: string;
  age: number;
  phoneNumber: string;
  civility: string;
  birthDate: Date;
  firstname: string;
  lastname: string;
  about: string;
  complainsNumber:number;
  photo: {
    id: number;
    fileUrl: string;
    fileUrlType: string;
  };
  email: string;
  role: string;
  address: {
    city: string;
    country: string;
    optionalInfo: string;
    region: string | null;
    streetAddress: string;
  };
  socialLinks: {
    facebook: string;
    linkedin: string;
    instagram: string;
    twitter: string;

  };
  verified: boolean;
  reviewsNumber: number;
}
