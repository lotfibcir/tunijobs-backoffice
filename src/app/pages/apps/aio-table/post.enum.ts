export enum RecruiterPost {
  SOFTWARE_ENGINEER = 'Software Engineer',
  WEB_DEVELOPER = 'Web Developer',
  DATA_ANALYST = 'Data Analyst',
  DATA_SCIENTIST = 'Data Scientist',
  PRODUCT_MANAGER = 'Product Manager',
  PROJECT_MANAGER = 'Project Manager',
  SALES_REPRESENTATIVE = 'Sales Representative',
  MARKETING_MANAGER = 'Marketing Manager',
  GRAPHIC_DESIGNER = 'Graphic Designer',
  CONTENT_WRITER = 'Content Writer',
  HUMAN_RESOURCES_MANAGER = 'Human Resources Manager',
  FINANCIAL_ANALYST = 'Financial Analyst',
  CUSTOMER_SUPPORT_SPECIALIST = 'Customer Support Specialist',
  BUSINESS_ANALYST = 'Business Analyst',
  ACCOUNTANT = 'Accountant',
  ADMINISTRATIVE_ASSISTANT = 'Administrative Assistant',
  OPERATIONS_MANAGER = 'Operations Manager',
  IT_SUPPORT_SPECIALIST = 'IT Support Specialist',
  SOFTWARE_ARCHITECT = 'Software Architect',
  UX_UI_DESIGNER = 'UX/UI Designer',
  QUALITY_ASSURANCE_ENGINEER = 'Quality Assurance (QA) Engineer',
  SYSTEMS_ADMINISTRATOR = 'Systems Administrator',
  NETWORK_ENGINEER = 'Network Engineer',
  DEVOPS_ENGINEER = 'DevOps Engineer',
  DIGITAL_MARKETING_SPECIALIST = 'Digital Marketing Specialist',
  SOCIAL_MEDIA_MANAGER = 'Social Media Manager',
  SALES_MANAGER = 'Sales Manager',
  HR_COORDINATOR = 'HR Coordinator',
  BUSINESS_DEVELOPMENT_MANAGER = 'Business Development Manager',
  SUPPLY_CHAIN_ANALYST = 'Supply Chain Analyst',
  LEGAL_COUNSEL = 'Legal Counsel',
  RESEARCH_SCIENTIST = 'Research Scientist',
  TEACHING_ASSISTANT = 'Teaching Assistant',
  EXECUTIVE_ASSISTANT = 'Executive Assistant',
  MECHANICAL_ENGINEER = 'Mechanical Engineer',
  ELECTRICAL_ENGINEER = 'Electrical Engineer',
  NURSE = 'Nurse',
  PHARMACIST = 'Pharmacist',
  DOCTOR = 'Doctor',
  PROJECT_COORDINATOR = 'Project Coordinator',
  DATA_ENTRY_CLERK = 'Data Entry Clerk',
  FINANCIAL_CONTROLLER = 'Financial Controller',
  EVENT_COORDINATOR = 'Event Coordinator',
  PUBLIC_RELATIONS_MANAGER = 'Public Relations Manager',
  SOFTWARE_TESTER = 'Software Tester',
  FRONT_END_DEVELOPER = 'Front-end Developer',
  BACK_END_DEVELOPER = 'Back-end Developer',
  FULL_STACK_DEVELOPER = 'Full-stack Developer',
  CUSTOMER_SUCCESS_MANAGER = 'Customer Success Manager',
  TECHNICAL_SUPPORT_SPECIALIST = 'Technical Support Specialist',
  DATA_ENGINEER = 'Data Engineer',
  BUSINESS_INTELLIGENCE_ANALYST = 'Business Intelligence Analyst',
  PRODUCT_DESIGNER = 'Product Designer',
  MOBILE_APP_DEVELOPER = 'Mobile App Developer',
  SALES_ASSOCIATE = 'Sales Associate',
  CONTENT_MARKETING_SPECIALIST = 'Content Marketing Specialist',
  HR_MANAGER = 'HR Manager',
  FINANCIAL_MANAGER = 'Financial Manager',
  CUSTOMER_SERVICE_REPRESENTATIVE = 'Customer Service Representative',
  SUPPLY_CHAIN_MANAGER = 'Supply Chain Manager',
  LEGAL_ASSISTANT = 'Legal Assistant',
  MARKET_RESEARCH_ANALYST = 'Market Research Analyst',
  SOFTWARE_SUPPORT_ENGINEER = 'Software Support Engineer',
  IT_SECURITY_SPECIALIST = 'IT Security Specialist',
  CLOUD_SOLUTIONS_ARCHITECT = 'Cloud Solutions Architect',
  ECOMMERCE_MANAGER = 'E-commerce Manager',
  SEO_SPECIALIST = 'SEO Specialist',
  BRAND_MANAGER = 'Brand Manager',
  PROJECT_ENGINEER = 'Project Engineer',
  CIVIL_ENGINEER = 'Civil Engineer',
  ARCHITECT = 'Architect',
  PHYSICAL_THERAPIST = 'Physical Therapist',
  OCCUPATIONAL_THERAPIST = 'Occupational Therapist',
  RADIOLOGIC_TECHNOLOGIST = 'Radiologic Technologist',
  DENTAL_HYGIENIST = 'Dental Hygienist',
  EVENT_PLANNER = 'Event Planner',
  SOCIAL_WORKER = 'Social Worker',
  PSYCHOLOGIST = 'Psychologist',
  COPYWRITER = 'Copywriter',
  UI_UX_DEVELOPER = 'UI/UX Developer',
  NETWORK_ADMINISTRATOR = 'Network Administrator',
  DATABASE_ADMINISTRATOR = 'Database Administrator',
  SYSTEMS_ANALYST = 'Systems Analyst',
  SALES_ENGINEER = 'Sales Engineer',
  LOGISTICS_COORDINATOR = 'Logistics Coordinator',
  PROCUREMENT_SPECIALIST = 'Procurement Specialist',
  COMPLIANCE_OFFICER = 'Compliance Officer',
  LABORATORY_TECHNICIAN = 'Laboratory Technician',
  RESEARCH_ASSISTANT = 'Research Assistant',
  PERSONAL_ASSISTANT = 'Personal Assistant',
  OPERATIONS_COORDINATOR = 'Operations Coordinator',
  PRODUCTION_MANAGER = 'Production Manager',
  VIDEO_EDITOR = 'Video Editor',
  GAME_DEVELOPER = 'Game Developer',
  MOBILE_APP_TESTER = 'Mobile App Tester',
  UI_UX_TESTER = 'UI/UX Tester',
  CYBERSECURITY_ANALYST = 'Cybersecurity Analyst',
  NETWORK_SECURITY_ENGINEER = 'Network Security Engineer',
  CUSTOMER_SERVICE_MANAGER = 'Customer Service Manager',
  TECHNICAL_WRITER = 'Technical Writer'
}
export const ListeDesMetiers: { value: string; label: string }[] = Object.keys(RecruiterPost).map(key => ({
  value: key,
  label: RecruiterPost[key]
}));

export const getNameForValue = (value: string): string | undefined => {
  const entry = ListeDesMetiers.find(item => item.value === value);
  return entry ? entry.label : '';
};
