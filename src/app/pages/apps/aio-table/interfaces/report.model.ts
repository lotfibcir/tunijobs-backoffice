export interface ReportModel {
  createdAt: Date;
  id: number;
  reason: string;
  reporter: {
    id:number;
    firstname: string;
    lastname: string;
    email: string;
    civility:string;
    role: string;
    photo: {
      id: number;
      fileUrl:string;
      fileUrlType: string;
    }
  },
  reported: {
    id:number;
    firstname: string;
    lastname: string;
    email:string;
    civility: string;
    role: string;
    photo: {
      id:number;
      fileUrl: string;
      fileUrlType: string;
    }
  },
  screenshot: {
    id: number;
    fileUrl: string;
    fileUrlType: string;
  }
}
