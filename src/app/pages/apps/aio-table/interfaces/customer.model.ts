export class Customer {
  id: number;
  imageSrc: string;
  firstName: string;
  lastName: string;
  street: string;
  city: string;
  phoneNumber: string;
  mail: string;
  post: string;
  civility: string;
  birthDate: Date;
  age: number;
  complainsNumber: number;
  role: string;
  banned: boolean;
  labels: any;

  constructor(customer) {
    this.id = customer.id;
    this.imageSrc = customer.imageSrc;
    this.firstName = customer.firstName;
    this.lastName = customer.lastName;
    this.street = customer.street;
    this.city = customer.city;
    this.phoneNumber = customer.phoneNumber;
    this.mail = customer.mail;
    this.labels = customer.labels;
    this.post = customer.post;
    this.banned = customer.banned;
    this.civility = customer.civility;
    this.birthDate = customer.birthDate;
    this.age = customer.age;
    this.role = customer.role;
    this.complainsNumber = customer.complainsNumber;
  }

  get name() {
    let name = '';

    if (this.firstName && this.lastName) {
      name = this.firstName + ' ' + this.lastName;
    } else if (this.firstName) {
      name = this.firstName;
    } else if (this.lastName) {
      name = this.lastName;
    }

    return name;
  }

  set name(value) {
  }

  get address() {
    return `${this.street}, ${this.city}`;
  }

  set address(value) {
  }
}
