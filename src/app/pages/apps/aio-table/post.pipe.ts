import { Pipe, PipeTransform } from '@angular/core';
import {getNameForValue} from "./post.enum";

@Pipe({
  name: 'post'
})
export class PostPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): unknown {
    return getNameForValue(value);
  }

}
