import {Component, Inject, OnInit} from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Customer} from '../interfaces/customer.model';
import {AuthService} from "../../../../security/auth.service";
import {AdminService} from "../../../../services/admin.service";
import {ReportService} from "../report.service";
import {ReportModel} from "../interfaces/report.model";
import {HttpStatusCode} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";

@Component({
  selector: 'vex-customer-create-update',
  templateUrl: './customer-create-update.component.html',
  styleUrls: ['./customer-create-update.component.scss']
})
export class CustomerCreateUpdateComponent implements OnInit {
  static id = 100;
  panelOpenState = false;
  isBanned = false;
  form: UntypedFormGroup;
  mode: 'create' | 'update' = 'create';
  repports: ReportModel[] = [];
  baseUrl = environment.baseUrl;

  constructor(@Inject(MAT_DIALOG_DATA) public defaults: any,
              private dialogRef: MatDialogRef<CustomerCreateUpdateComponent>,
              private fb: UntypedFormBuilder, private authService: AuthService,
              private adminService: AdminService,
              private reportsService: ReportService) {
  }

  ngOnInit() {
    if (this.defaults) {
      this.mode = 'update';
    } else {
      this.defaults = {} as Customer;
    }
    console.log(this.defaults);
    this.isBanned = this.defaults.banned;
    this.form = this.fb.group({
      id: [CustomerCreateUpdateComponent.id++],
      imageSrc: this.defaults.imageSrc,
      firstName: [this.defaults.firstName || ''],
      lastName: [this.defaults.lastName || ''],
      email: [this.defaults.mail || ''],
      street: this.defaults.street || '',
      city: this.defaults.city || '',
      phoneNumber: this.defaults.phoneNumber || '',
      notes: this.defaults.notes || '',
      role: this.defaults?.role ,
      age: this.defaults?.age,
      birthDate: this.defaults?.birthDate,
      civility: this.defaults?.civility,
    });
    this.getReports();
  }

  getReports() {
    this.reportsService.getReportsByReporter(this.defaults?.id).subscribe({
      next: res => {
        console.log(res);
        if (res.status === HttpStatusCode.Ok) {
          this.repports = res.body;
        } else if (res.status === 204) {
          this.repports = [];
        }
      }, error: err => {
        console.log(err);
        this.repports = [];
      }
    })
  }

  save() {
    if (this.mode === 'create') {
      this.createCustomer();
    } else if (this.mode === 'update') {
      this.updateCustomer();
    }
  }

  createCustomer() {
    const customer = this.form.value;

    if (!customer.imageSrc) {
      customer.imageSrc = 'assets/img/avatars/1.jpg';
    }

    this.dialogRef.close(customer);
  }

  //TODO complete update user
  updateCustomer() {
    const customer = this.form.value;
    customer.id = this.defaults.id;
    const request = this.form.value;
    console.log(request);
    this.dialogRef.close(customer);
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  banUser(): void {
    this.adminService.banUser(this.defaults?.id).subscribe({
      next: res => {
        this.isBanned = true;
      }, error: err => {
        console.log(err);
        this.isBanned = false;
      }
    })
  }

  unBannUser(): void {
    this.adminService.unBanUser(this.defaults?.id).subscribe({
      next: res => {
        this.isBanned = false;
      }, error: err => {
        console.log(err);
        this.isBanned = true;
      }
    })
  }

  deleteReport(id) {
    this.reportsService.deleteReport(id).subscribe({
      next: res => {
        this.getReports();
      }, error: err => {
        console.log(err);
      }
    })
  }
}
