import {Injectable} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {ReportModel} from "./interfaces/report.model";

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getReportsByReporter(id) {
    return this.http.get<ReportModel[]>(`${this.baseUrl}/users/report/reported/${id}`, {observe: 'response'});
  }

  getAllReports() {
    return this.http.get<ReportModel[]>(`${this.baseUrl}/users/report`, {observe: 'response'});
  }

  getReportById(id) {
    return this.http.get<ReportModel>(`${this.baseUrl}/users/report/${id}`, {observe: 'response'});
  }

  deleteReport(id) {
   return  this.http.delete(`${this.baseUrl}/users/report/${id}`, {observe: 'response'});
  }
}
