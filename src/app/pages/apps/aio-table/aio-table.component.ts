import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {Observable, ReplaySubject} from 'rxjs';
import {filter} from 'rxjs/operators';
import {Customer} from './interfaces/customer.model';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatDialog} from '@angular/material/dialog';
import {TableColumn} from '../../../../@vex/interfaces/table-column.interface';
import {aioTableLabels} from '../../../../static-data/aio-table-data';
import {CustomerCreateUpdateComponent} from './customer-create-update/customer-create-update.component';
import {SelectionModel} from '@angular/cdk/collections';
import {fadeInUp400ms} from '../../../../@vex/animations/fade-in-up.animation';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldDefaultOptions} from '@angular/material/form-field';
import {stagger40ms} from '../../../../@vex/animations/stagger.animation';
import {UntypedFormControl} from '@angular/forms';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {MatSelectChange} from '@angular/material/select';
import {AdminService} from "../../../services/admin.service";
import {HttpStatusCode} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {USER_ROLES} from "../../../security/user_roles.constants";
import {getNameForValue} from "./post.enum";
import {ReportService} from "./report.service";


@UntilDestroy()
@Component({
  selector: 'vex-aio-table',
  templateUrl: './aio-table.component.html',
  styleUrls: ['./aio-table.component.scss'],
  animations: [
    fadeInUp400ms,
    stagger40ms
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'standard'
      } as MatFormFieldDefaultOptions
    }
  ]
})
export class AioTableComponent implements OnInit, AfterViewInit {
  baseUrl = environment.baseUrl;
  layoutCtrl = new UntypedFormControl('boxed');
  /**
   * Simulating a service with HTTP that returns Observables
   * You probably want to remove this and do all requests in a service with HTTP
   */
  subject$: ReplaySubject<Customer[]> = new ReplaySubject<Customer[]>(1);
  data$: Observable<Customer[]> = this.subject$.asObservable();
  customers: Customer[];

  @Input()
  columns: TableColumn<Customer>[] = [
    {label: 'Checkbox', property: 'checkbox', type: 'checkbox', visible: true},
    {label: 'Image', property: 'image', type: 'image', visible: true},
    {label: 'Name', property: 'name', type: 'text', visible: false, cssClasses: ['font-medium']},
    {label: 'First Name', property: 'firstName', type: 'text', visible: true},
    {label: 'Last Name', property: 'lastName', type: 'text', visible: true},
    {label: 'Post', property: 'post', type: 'text', visible: true},
    {label: 'Contact', property: 'contact', type: 'button', visible: false},
    {
      label: 'Address',
      property: 'address',
      type: 'text',
      visible: false,
      cssClasses: ['text-secondary', 'font-medium']
    },
    {label: 'region', property: 'street', type: 'text', visible: true, cssClasses: ['text-secondary', 'font-medium']},
    {label: 'City', property: 'city', type: 'text', visible: true, cssClasses: ['text-secondary', 'font-medium']},
    {
      label: 'Phone',
      property: 'phoneNumber',
      type: 'text',
      visible: true,
      cssClasses: ['text-secondary', 'font-medium']
    },
    {label: 'Labels', property: 'labels', type: 'button', visible: false},
    {label: 'Actions', property: 'actions', type: 'button', visible: true}
  ];
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 20, 50];
  dataSource: MatTableDataSource<Customer> | null;
  selection = new SelectionModel<Customer>(true, []);
  searchCtrl = new UntypedFormControl();

  labels = aioTableLabels;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private dialog: MatDialog,
              private adminService: AdminService,
              private reportService: ReportService) {
  }

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngOnInit() {
    let result: Customer[] = [];
    this.adminService.getUsers().subscribe({
      next: res => {
        if (res.status === HttpStatusCode.Ok) {
          res.body.forEach(user => {
            const customer: Customer = {} as Customer;
            customer.id = +user?.id;
            customer.firstName = user?.firstname;
            customer.lastName = user?.lastname;
            customer.imageSrc = this.baseUrl + user.photo?.fileUrl;
            customer.phoneNumber = user?.phoneNumber;
            customer.mail = user?.email;
            customer.city = user?.address?.city;
            customer.street = user?.address?.streetAddress;
            customer.banned = user?.banned;
            customer.role = user?.role;
            customer.age = user?.age;
            customer.birthDate = user?.birthDate;
            customer.civility = user?.civility;
            customer.complainsNumber = user?.complainsNumber;
            if (user.role === 'ROLE_CANDIDATE') {
              customer.post = user?.candidate?.post || 'looking for job';
            } else if (user.role === USER_ROLES.ROLE_RECRUITER) {
              customer.post = getNameForValue(user?.recruiter?.post)
            } else {
              customer.post = 'empty';
            }
            result.push(customer);
          })
        }
        this.subject$.next(result);
      }, error: err => console.error(err)
    })
    this.dataSource = new MatTableDataSource();

    this.data$.pipe(
      filter<Customer[]>(Boolean)
    ).subscribe(customers => {
      this.customers = customers;
      this.dataSource.data = customers;
    });

    this.searchCtrl.valueChanges.pipe(
      untilDestroyed(this)
    ).subscribe(value => this.onFilterChange(value));
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  createCustomer() {
    this.dialog.open(CustomerCreateUpdateComponent).afterClosed().subscribe((customer: Customer) => {
      /**
       * Customer is the updated customer (if the user pressed Save - otherwise it's null)
       */
      if (customer) {
        /**
         * Here we are updating our local array.
         * You would probably make an HTTP request here.
         */
        this.customers.unshift(new Customer(customer));
        this.subject$.next(this.customers);
      }
    });
  }

  updateCustomer(customer: Customer) {
    this.dialog.open(CustomerCreateUpdateComponent, {
      data: customer
    }).afterClosed().subscribe(
      () => {
        this.ngOnInit();
      }
    )
  }

  deleteCustomer(customer: Customer) {
    /**
     * Here we are updating our local array.
     * You would probably make an HTTP request here.
     */
    this.customers.splice(this.customers.findIndex((existingCustomer) => existingCustomer.id === customer.id), 1);
    this.selection.deselect(customer);
    this.subject$.next(this.customers);
  }

  deleteCustomers(customers: Customer[]) {
    /**
     * Here we are updating our local array.
     * You would probably make an HTTP request here.
     */
    customers.forEach(c => this.deleteCustomer(c));
  }

  onFilterChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  trackByProperty<T>(index: number, column: TableColumn<T>) {
    return column.property;
  }

  onLabelChange(change: MatSelectChange, row: Customer) {
    const index = this.customers.findIndex(c => c === row);
    this.customers[index].labels = change.value;
    this.subject$.next(this.customers);
  }
}
