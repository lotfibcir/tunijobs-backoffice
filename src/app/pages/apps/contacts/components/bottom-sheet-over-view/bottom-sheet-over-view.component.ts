import {Component, Inject, OnInit} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from "@angular/material/bottom-sheet";

@Component({
  selector: 'vex-bottom-sheet-over-view',
  templateUrl: './bottom-sheet-over-view.component.html',
  styleUrls: ['./bottom-sheet-over-view.component.scss'],
})
export class BottomSheetOverViewComponent implements OnInit {

  constructor(private _bottomSheetRef: MatBottomSheetRef<BottomSheetOverViewComponent>,
              @Inject(MAT_BOTTOM_SHEET_DATA) public data: {
                reporterID: any,
                reporterName: string,
                reporterRole: string
              }) {
  }

  ngOnInit(): void {
  }


  openLink(event: MouseEvent): void {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }

}
