import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UntypedFormBuilder} from '@angular/forms';
import {Contact} from '../../interfaces/contact.interface';
import {ReportService} from "../../../aio-table/report.service";
import {HttpStatusCode} from "@angular/common/http";
import {DatePipe} from "@angular/common";
import {MatBottomSheet} from "@angular/material/bottom-sheet";
import {BottomSheetOverViewComponent} from "../bottom-sheet-over-view/bottom-sheet-over-view.component";
import {environment} from "../../../../../../environments/environment";

export let contactIdCounter = 50;

@Component({
  selector: 'vex-contacts-edit',
  templateUrl: './contacts-edit.component.html',
  styleUrls: ['./contacts-edit.component.scss']
})
export class ContactsEditComponent implements OnInit {
  baseUrl = environment.baseUrl;
  screenshot;
  form = this.fb.group({
    createdAt: null,
    id: null,
    reason: null,
    imageSrc: null,
    reporterId: null,
    reporterName: null,
    reporterRole: null,
    reportedId: null,
    reportedName: null,
    reportedRole: null,
    notes: null,
    birthday: null
  });

  contact: Contact;

  constructor(@Inject(MAT_DIALOG_DATA) private contactId: Contact['id'],
              private dialogRef: MatDialogRef<ContactsEditComponent>,
              private fb: UntypedFormBuilder,
              private reportService: ReportService,
              private datePipe: DatePipe,
              private _bottomSheet: MatBottomSheet) {
  }

  get isEdit(): boolean {
    return !!this.contactId;
  }

  ngOnInit() {
    this.reportService.getReportById(this.contactId).subscribe({
      next: res => {
        if (res.status === HttpStatusCode.Ok) {
          const contact: Contact = {} as Contact;
          const report = res.body;
          contact.id = report.id;
          contact.createdAt = this.datePipe.transform(new Date(report?.createdAt), 'MMM-dd-yyyy').toString();
          contact.imageSrc = report?.screenshot?.fileUrl;
          this.screenshot = report?.screenshot?.fileUrl;
          contact.reporterName = report?.reporter?.firstname + ' ' + report?.reporter?.lastname;
          contact.reporterEmail = report.reporter?.email;
          contact.reporterId = report.reporter?.id;
          contact.reporterRole = report.reporter.role === 'ROLE_CANDIDATE' ? 'candidate' : 'recruiter';
          contact.reportedName = report.reported?.firstname + ' ' + report.reported?.lastname;
          contact.reportedEmail = report?.reported?.email;
          contact.reportedId = report?.reported?.id;
          contact.reportedRole = report.reported.role === 'ROLE_CANDIDATE' ? 'candidate' : 'recruiter';
          contact.reason = report?.reason;
          this.form.patchValue(contact);
          console.log(this.form.value);
        }
      }, error: err => console.log(err)
    })
  }

  toggleStar() {
    if (this.contact) {
      this.contact.starred = !this.contact.starred;
    }
  }

  save() {
    const form = this.form.value;

    if (!this.contact) {
      this.contact = {
        ...form,
        id: contactIdCounter++
      };
    }

    this.contact.name = form.name;
    this.contact.email = form.email;
    this.contact.phone = form.phone;
    this.contact.company = form.company;
    this.contact.notes = form.notes;
    this.contact.birthday = form.birthday;

    this.dialogRef.close();
  }

  openBottomSheet(id: any, name: string, role: string) {
    const bottomSheetRef = this._bottomSheet.open(BottomSheetOverViewComponent, {
      data: {reporterID: id, reporterName: name, reporterRole: role}
    });
    bottomSheetRef.afterDismissed().subscribe(result => {
      // Handle the result (if any) when the bottom sheet is dismissed
      console.log('Bottom sheet dismissed with result:', result);
    });
  }

  deleteReport(id) {
    this.reportService.deleteReport(id).subscribe({
      next: () => {
        this.dialogRef.close();
      }, error: err => {
        console.log(err);
        this.dialogRef.close();
      }
    })
  }
}
