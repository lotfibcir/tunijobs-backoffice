export interface Contact {
  id: number;
  createdAt: string;
  imageSrc: string;
  reporterName: string;
  reporterEmail: string;
  reporterId: number;
  reporterRole: string;
  reportedName: string;
  reportedEmail: string;
  reportedId: number;
  reportedRole: string;
  reason?: string;
  birthday?: string;
  selected: boolean;
  starred: boolean;
  notes?: string;
  company?: string;
  phone?: string;
  email?: string;
  name?: string;
}
