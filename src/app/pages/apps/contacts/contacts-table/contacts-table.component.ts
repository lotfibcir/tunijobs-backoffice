import {Component, OnInit} from '@angular/core';
import {scaleIn400ms} from '../../../../../@vex/animations/scale-in.animation';
import {fadeInRight400ms} from '../../../../../@vex/animations/fade-in-right.animation';
import {TableColumn} from '../../../../../@vex/interfaces/table-column.interface';
import {UntypedFormControl} from '@angular/forms';
import {debounceTime} from 'rxjs/operators';
import {stagger40ms} from '../../../../../@vex/animations/stagger.animation';
import {MatDialog} from '@angular/material/dialog';
import {ContactsEditComponent} from '../components/contacts-edit/contacts-edit.component';
import {Contact} from '../interfaces/contact.interface';
import {ReportService} from "../../aio-table/report.service";
import {HttpStatusCode} from "@angular/common/http";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'vex-contacts-table',
  templateUrl: './contacts-table.component.html',
  animations: [
    stagger40ms,
    scaleIn400ms,
    fadeInRight400ms
  ],
  styles: [`
    .mat-drawer-container {
      background: transparent !important;
    }
  `]
})
export class ContactsTableComponent implements OnInit {

  searchCtrl = new UntypedFormControl();

  searchStr$ = this.searchCtrl.valueChanges.pipe(
    debounceTime(10)
  );

  menuOpen = false;

  activeCategory: 'frequently' | 'starred' | 'all' | 'family' | 'friends' | 'colleagues' | 'business' = 'all';
  tableData: Contact[] = [];
  tableColumns: TableColumn<Contact>[] = [
    {
      label: '',
      property: 'selected',
      type: 'checkbox',
      cssClasses: ['w-6']
    },
    {
      label: 'ID',
      property: 'id',
      type: 'text',
      cssClasses: ['min-w-9']
    },
    {
      label: 'REPORTER',
      property: 'reporterName',
      type: 'text',
      cssClasses: ['font-medium']
    },
    {
      label: 'REPORTED USER',
      property: 'reportedName',
      type: 'text',
      cssClasses: ['font-medium']
    },
    {
      label: 'DATE',
      property: 'createdAt',
      type: 'text',
      cssClasses: ['text-secondary']
    },
    {
      label: '',
      property: 'menu',
      type: 'button',
      cssClasses: ['text-secondary', 'w-10']
    },
  ];

  constructor(private dialog: MatDialog,
              private reportService: ReportService,
              private datePipe: DatePipe) {
  }

  ngOnInit() {
    this.reportService.getAllReports().subscribe({
      next: res => {
        const reports: Contact[] = [];
        if (res.status === HttpStatusCode.Ok) {
          res.body.forEach(report => {
            const contact: Contact = {} as Contact;
            contact.id = report?.id;
            contact.createdAt = this.datePipe.transform(new Date(report?.createdAt), 'MMM-dd-yyyy').toString();
            contact.reporterName = report?.reporter?.firstname + ' ' + report?.reporter?.lastname;
            contact.reporterEmail = report?.reporter?.email;
            contact.reporterId = report?.reporter?.id;
            contact.reportedName = report?.reported?.firstname + ' ' + report?.reported?.lastname;
            contact.reportedEmail = report?.reported?.email;
            contact.reportedId = report?.reported?.id;
            contact.imageSrc = report?.screenshot?.fileUrl;
            contact.reason = report?.reason;
            reports.push(contact);
          })
          this.tableData = reports;
        }
      }, error: err => {
        console.log(err);
      }
    })
  }

  openContact(id?: Contact['id']) {
    this.dialog.open(ContactsEditComponent, {
      data: id || null,
      width: '600px'
    }).afterClosed().subscribe(
      () => {
        this.ngOnInit();
      }
    )
  }

  toggleStar(id: Contact['id']) {
    const contact = this.tableData.find(c => c.id === id);
    //
    // if (contact) {
    //   contact.starred = !contact.starred;
    // }
  }

  setData(data: Contact[]) {
    this.tableData = data;
    this.menuOpen = false;
  }

  openMenu() {
    this.menuOpen = true;
  }
}
