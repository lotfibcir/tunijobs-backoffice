import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {AppUser} from "../security/appUser.model";

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {
  }

  getUsers() {
    return this.http.get<AppUser[]>(`${this.baseUrl}/api/admin/users`, {observe: 'response'});
  }

  banUser(id) {
    return this.http.patch<boolean>(`${this.baseUrl}/api/admin/bann/${id}`, {}, {observe: 'response'});
  }

  unBanUser(id) {
    return this.http.patch<boolean>(`${this.baseUrl}/api/admin/unbann/${id}`, {}, {observe: 'response'});
  }
}
