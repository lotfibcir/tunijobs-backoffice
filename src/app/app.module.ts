import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VexModule } from '../@vex/vex.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { CustomLayoutModule } from './custom-layout/custom-layout.module';
import {AuthInterceptor} from "./security/auth.interceptor";
import { PostPipe } from './pages/apps/aio-table/post.pipe';
import {DatePipe} from "@angular/common";
import { BottomSheetOverViewComponent } from './pages/apps/contacts/components/bottom-sheet-over-view/bottom-sheet-over-view.component';
import {MatListModule} from "@angular/material/list";

@NgModule({
  declarations: [AppComponent, PostPipe, BottomSheetOverViewComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,

    // Vex
    VexModule,
    CustomLayoutModule,
    MatListModule
  ],
  providers: [ {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
