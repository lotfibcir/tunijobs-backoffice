import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable, of, tap} from "rxjs";
import {AppUser} from "./appUser.model";
import {Token} from "./token.model";
import {environment} from "../../environments/environment";
import {StorageService} from "./storage.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authenticatedUser: AppUser | undefined;
  public _errorField = new BehaviorSubject(null);
  errorField = this._errorField.asObservable();
  private baseUrl = environment.baseUrl;
  private _isLoggedIn$ = new BehaviorSubject<boolean>(false);
  isLoggedIn$ = this._isLoggedIn$.asObservable();

  constructor(private http: HttpClient, private storage: StorageService) {
    const token = localStorage.getItem('token');
    this._isLoggedIn$.next(!!token);
  }

  login(email: string, password: string) {
    return this.http.post<Token>(`${this.baseUrl}/api/v1/auth/authenticate`, {
      email,
      password
    }, {observe: 'response'}).pipe(tap({
        next: res => {
          if (res.status === 200) {
            if (res.body.access_token) {
              localStorage.setItem('token', res.body.access_token);
              localStorage.setItem('refresh_token', res.body.refresh_token);
              this._isLoggedIn$.next(true);
              this._errorField.next(null);
              this.authenticateUser();
            }
          }
        },
        error: err => {
          localStorage.clear();
        }
      }
    ));
  }

  logout(): Observable<boolean> {
    this.http.post(`${this.baseUrl}/api/v1/auth/logout`, {});
    localStorage.clear();
    return of(true);
  }

  authenticateUser(): Observable<boolean> {
    this.http.get(`${this.baseUrl}/api/auth/me`).subscribe({
      next: (res: any) => {
        localStorage.setItem('authenticatedUser', JSON.stringify({
          email: res.email,
          firstname: res.firstname,
          lastname: res.lastname,
          role: res.role,
          verified: res.verified,
          banned: res.banned
        }));
      }, error: err => {
        console.log(err);
      }
    });
    return of(true);
  }

  isAuthenticated(): boolean {
    return !!localStorage.getItem('token');
  }

  getAuthenticatedUser(): Observable<AppUser> {
    return this.http.get<AppUser>(`${this.baseUrl}/api/auth/me`);
  }

  refreshToken() {
    const headers = this.storage.getRefreshtoken();
    return this.http.post(`${this.baseUrl}/api/v1/auth/refresh-token`, {}, {headers});
  }

  getUser(id) {
    return this.http.get<AppUser>(`${this.baseUrl}/users/user-detail/${id}`)
  }
}
