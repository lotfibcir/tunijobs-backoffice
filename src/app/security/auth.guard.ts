import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, of} from 'rxjs';
import {AuthService} from "./auth.service";
import {TokenService} from "./token.service";
import {catchError, map} from 'rxjs/operators';
import {Token} from "./token.model";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService,
              private router: Router,
              private tokenService: TokenService) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const user: any = localStorage.getItem('authenticatedUser');
    const authUser = JSON.parse(user);
    const token = localStorage.getItem('token');
    const refreshToken = localStorage.getItem('refresh_token');
    if (!token) {
      this.router.navigate(['/login']);
      return false;
    }
    if (token && this.tokenService.isTokenExpired(token)) {
      if (refreshToken) {
        return this.authService.refreshToken().pipe(
          map((res: Token) => {
            localStorage.setItem('refresh_token', res.refresh_token);
            localStorage.setItem('token', res.access_token);
            return true; // Continue with the route
          }),
          catchError(err => {
            console.log(err);
            this.authService.logout().subscribe({
              next: res => console.log('Logout success'),
              error: err1 => console.log('Logout failed')
            });
              this.router.navigate(['/login']);
            return of(false); // Block the route
          })
        );
      } else {
          this.router.navigate(['/login']);
        return false;
      }
    }
    return true;
  }

}
