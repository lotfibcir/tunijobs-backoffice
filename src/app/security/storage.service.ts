import { Injectable } from '@angular/core';
import {Token} from "./token.model";

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() {
  }

  storeToken(token: Token) {
    localStorage.setItem('userData', JSON.stringify(token));
  }

  storeData(item, data) {
    localStorage.setItem(item, JSON.stringify(data));
  }

  isAuthenticated() {
    return !!localStorage.getItem('userData');
  }

  clearToken() {
    localStorage.removeItem('UserData');
  }

  getToken(): { Authorization: string } {
    const token = localStorage.getItem('token') || '';
    if (!token) {
      return null;
    }
    return {
      Authorization: `Bearer ${token}`
    };
  }

  getRefreshtoken() {
    const token = localStorage.getItem('refresh_token') || '';
    if (!token) {
      return null;
    }
    return {
      Authorization: `Bearer ${token}`
    };
  }
}
