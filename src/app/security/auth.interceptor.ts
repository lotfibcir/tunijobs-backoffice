import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable, tap} from "rxjs";
import {StorageService} from "./storage.service";
import {AuthService} from "./auth.service";
import {environment} from "../../environments/environment";
import {Router} from "@angular/router";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  baseUrl = environment.baseUrl;

  constructor(private router: Router, private storage: StorageService, private auth: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const headers = this.storage.getToken();

    if (request.url === `${this.baseUrl}/api/auth/me`) {
      if (headers) {
        const newRequest = request.clone({setHeaders: {Authorization: headers.Authorization}});
        return next.handle(newRequest).pipe(tap((event) => {
        }, e => {
          this.auth.logout().subscribe();
          localStorage.clear();
          setTimeout(() => {
            this.router.navigate(['/login']);
          }, 3000);
        }));
      }
    }
    if (headers) {
      const authedRequest = request.clone({setHeaders: {Authorization: headers.Authorization}});
      return next.handle(authedRequest);
    }
    return next.handle(request);
  }

}
