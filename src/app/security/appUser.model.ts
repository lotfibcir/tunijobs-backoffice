export interface AppUser {
  id: string;
  age: number;
  phoneNumber: string;
  civility: string;
  birthDate: Date;
  firstname: string;
  lastname: string;
  complainsNumber: number;
  photo: {
    id: number;
    fileUrl: string;
    fileUrlType: string;
  };
  email: string;
  role: string;
  address: {
    city: string;
    country: string;
    optionalInfo: string;
    region: string | null;
    streetAddress: string;
  };
  candidate: {
    authoriseNotif: boolean;
    salary: number;
    status: string;
    studyLevel: string;
    experienceLevel: string;
    post: string;
    cachFiles: boolean;
    company: {
      ceo: string;
      email: string;
      foundedYear: Date;
      id: number;
      identifientUnique: string;
      logo: {
        id: number;
        fileUrl: string;
        fileUrlType: string;
      };
      name: string;
      numberOfEmployees: number;
      phoneNumber: string;
      website: string;
      industry: string;
    };
  };
  verified: boolean;
  reviewsNumber: number;
  recruiter: {
    post: string;
    score: string;
    authoriseNotif: boolean;
    company: {
      address: {
        city: string;
        country: string;
        optionalInfo: string;
        region: string | null;
        streetAddress: string;
      };
      ceo: string;
      email: string;
      foundedYear: Date;
      id: number;
      identifientUnique: string;
      logo: {
        id: number;
        fileUrl: string;
        fileUrlType: string;
      };
      name: string;
      numberOfEmployees: number;
      phoneNumber: string;
      website: string;
      industry: string;
    };
  };
  banned: boolean;
}
